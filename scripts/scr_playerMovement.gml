///Player Moving Script

//Direction Degrees
//Right : 0
//Left : 180
//Up : 90
//Down : 270

keyup = keyboard_check(vk_up) or keyboard_check(ord('W'))
keydown = keyboard_check(vk_down) or keyboard_check(ord('S'))
keyright = keyboard_check(vk_right) or keyboard_check(ord('D'))
keyleft = keyboard_check(vk_left) or keyboard_check(ord('A'))

movementspeed = 3

//Player Rotation
image_angle=point_direction(x,y,mouse_x,mouse_y)
direction = image_angle;

//Player Movement
if (keyup == true) {
    direction = image_angle;
    if(speed != 3){
        speed+=movementspeed;
    }
}
if (keydown == true){
    direction = image_angle;
    if(speed != -3){
        speed-=movementspeed;
    }
}

if (keyup == false and keydown == false) {
    speed = 0;
}

//if (keyright == true) {
//    direction = direction + 90
//    motion_set(direction, movementspeed)
//}
