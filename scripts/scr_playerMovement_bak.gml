///Player Moving Script

//Direction Degrees
//Right : 0
//Left : 180
//Up : 90
//Down : 270

rightkey = keyboard_check(vk_right);
rightkey0 = keyboard_check_released(vk_right);
leftkey = keyboard_check(vk_left);
leftkey0 = keyboard_check_released(vk_left);
upkey = keyboard_check(vk_up or ord('W'));
upkey0 = keyboard_check_released(vk_up);
downkey = keyboard_check(vk_down);
downkey0 = keyboard_check_released(vk_down);

movementspeed = 3

if (keyboard_check(vk_left) or keyboard_check(ord('A'))){
    x-=movementspeed;
}
if (keyboard_check(vk_right) or keyboard_check(ord('D'))){
    x+=movementspeed;
}
if (keyboard_check(vk_up) or keyboard_check(ord('W'))){
    y-=movementspeed;
}
if (keyboard_check(vk_down) or keyboard_check(ord('S'))){
    y+=movementspeed;
}

//Player Rotation
image_angle=point_direction(x,y,mouse_x,mouse_y)